import XMonad
import Control.Monad (forM_, join)
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.Map as M
import XMonad.Config.Desktop
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Layout.Spacing
import XMonad.StackSet as W
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run (safeSpawn)
import XMonad.Util.SpawnOnce

import Graphics.X11.ExtraTypes.XF86

myStartupHook = do
    spawnOnce "$HOME/.config/polybar/launch.sh"
    spawnOnce "nitrogen --restore"
    spawnOnce "light-locker"
    spawnOnce "picom -b --unredir-if-possible"

myTerminal = "alacritty"
myModMask = mod1Mask -- Alt; use mod4Mask for the Windows key

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((myModMask, xK_f), (spawn "firefox &"))
    , ((myModMask, xK_p), (spawn "dmenu_run -fn \"Monoid Retina-16\""))
    , ((mod4Mask,  xK_l), (spawn "light-locker-command -l"))
    ]

eventLogHook = do
    winset <- gets windowset
    title <- maybe (return "") (fmap show . getName) . W.peek $ winset
    let currWs = W.currentTag winset
    let wss = map W.tag $ W.workspaces winset
    let wsStr = join $ map (fmt currWs) $ sort' wss
  
    io $ appendFile "/tmp/.xmonad-title-log" (title ++ "\n")
    io $ appendFile "/tmp/.xmonad-workspace-log" (wsStr ++ "\n")
  
    where fmt currWs ws
            | currWs == ws = "[" ++ ws ++ "]"
            | otherwise    = " " ++ ws ++ " "
          sort' = sortBy (compare `on` (!! 0))

main = xmonad . ewmh $ desktopConfig
    { terminal = "alacritty"
    , borderWidth = 0
    , modMask = myModMask
    , keys = \c -> myKeys c `M.union` keys XMonad.def c
    , layoutHook = avoidStruts $ smartSpacing 5 $ layoutHook defaultConfig
    , logHook = eventLogHook
    , manageHook = manageHook defaultConfig <+> manageDocks
    , startupHook = myStartupHook >> ewmhDesktopsStartup <+> startupHook desktopConfig
    , handleEventHook = fullscreenEventHook <+> ewmhDesktopsEventHook
    }
