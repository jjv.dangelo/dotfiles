" Borrowed heavily from https://github.com/jonhoo/configs/blob/master/editor/.config/nvim/init.vim
set nocompatible
filetype off

call plug#begin()

Plug 'itchyny/lightline.vim'
Plug 'jaredgorski/fogbell.vim'
Plug 'atelierbram/Base2Tone-vim'
Plug 'owickstrom/vim-colors-paramount'

Plug 'tpope/vim-obsession'
Plug 'vim-syntastic/syntastic'
Plug 'rust-lang/rust.vim'
Plug 'preservim/tagbar'

Plug 'airblade/vim-rooter'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'christoomey/vim-tmux-navigator'
Plug 'benmills/vimux'

call plug#end()

let mapleader = "\<Space>"

if has('nvim')
    set guicursor=n-v-c:block-Cursor/lCursor-blinkon0,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor
    set inccommand=nosplit
    noremap <C-q> :confirm qall<CR>
end

if !has('gui_running')
	set t_Co=256
endif

if (match($TERM, "-256color") != -1) && (match($TERM, "screen-256color") == -1)
    " screen does not (yet) support truecolor
    set termguicolors
endif

" Colors
set background=dark
"colo base16-ashes
colo Base2Tone_EarthDark

" See https://github.com/mike-hearn/base16-vim-lightline/tree/master/autoload/lightline/colorscheme
" let g:lightline = { 'colorscheme': 'base16_ashes' }
let g:lightline = { 'colorscheme': 'Base2Tone_Earth' }

" Set Syntastic up with the defaults
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Format Rust files on save
let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0

" Use a blinking line when in insert and blinking block in normal/visual mode
" let &t_SI = "\e[5 q"
" let &t_EI = "\e[1 q"

" Give vim access to .bashrc, etc.
set shellcmdflag=-c

syntax on

let base16colorspace=256

if executable('rg')
    command! -bang -nargs=* 
                \ Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>),
                \ 1, {'options': '--delimiter : --nth 4..'}, <bang>0)
    set grepprg=rg\ --vimgrep\ --smart-case\ --follow
    " set grepformat=%f:%l:%c:%m
endif

set cmdheight=2
set updatetime=300

filetype plugin indent on

set timeoutlen=300
set encoding=utf-8
set noshowmode

set splitright
set splitbelow

set wildmenu
set showmode
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set shiftround
set backspace=indent,eol,start
set autoindent
set copyindent
set number
set relativenumber

set showmatch
set ignorecase
set smartcase
set smarttab
set scrolloff=4
set hlsearch
set incsearch
set gdefault
set nolist
set autoread
set hidden

set history=1000
set undolevels=1000

set nobackup
set noswapfile

"set directory=~/.vim/.tmp,~/tmp,/tmp
"set viminfo='20,\"80
set wildmenu
set wildmode=list:full
set wildignore=*.swp,*.bak,*.pyc,*.class,Session.vim
set title

"no error bells/flash
set visualbell
set t_vb=
set noerrorbells

set showcmd
set nomodeline

set mouse=a

" Permanent undo
set undodir=~/.vimdid
set undofile

inoremap jj <Esc>
inoremap jk <Esc>
inoremap kj <Esc>

nnoremap <leader>w :w<CR>
nnoremap <leader>W :wa<CR>
nnoremap <silent> <C-f> :Rg<CR>
nnoremap <silent> <leader>f :Files<CR>
nnoremap <silent> <leader>t :Tags<CR>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
tnoremap <C-J> <C-\><C-n><C-W><C-J>
tnoremap <C-K> <C-\><C-n><C-W><C-K>
tnoremap <C-L> <C-\><C-n><C-W><C-L>
tnoremap <C-H> <C-\><C-n><C-W><C-H>
nnoremap j gj
nnoremap k gk

nnoremap <silent> <Right> :bn<CR>
nnoremap <silent> <Left> :bp<CR>
nnoremap <silent> <leader>d :bd<CR>

" Ctrl+h to stop searching
vnoremap <silent> <leader>h :nohlsearch<CR>
nnoremap <silent> <leader>h :nohlsearch<CR>

" Jump to start and end of line using the home row keys
map H g^
map L g$

" <leader><leader> toggles between buffers
nnoremap <leader><leader> <c-^>

" Building and error navigation
noremap <silent> <F5> :make run<CR>
noremap <silent> <F29> :make run --release<CR>
noremap <silent> <F6> :make check<CR><CR>
noremap <silent> <F8> :cnext<CR>
noremap <silent> <F20> :cprev<CR>

" Prompt for a command to run
map <leader>vp :VimuxPromptCommand<CR>

" Run last command executed by VimuxRunCommand
map <leader>vl :VimuxRunLastCommand<CR>
