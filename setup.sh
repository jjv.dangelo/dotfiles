#!/bin/bash

# Setup the symlink for init.vim
function setupNvim () {
    if [ -f /bin/nvim ];
    then
        echo nvim already installed.
    else
        echo Installing nvim.
        sudo pacman -S neovim
    fi

    local vimplug=$HOME/.local/share/nvim/site/autoload/plug.vim

    if [ -f $vimplug ];
    then
        echo vimplug already installed. Skipping.
    else
        echo Installing vimplug.
        local vimpluggit=https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

        curl -fLo $vimplug --create-dirs $vimpluggit
    fi

    local initvim="$PWD/init.vim"
    local nvimcfg="$HOME/.config/nvim"

    if [ -e $initvim ];
    then
        echo init.vim exists. Creating a symlink.
        ln -sf $initvim $nvimcfg  
    else
        echo init.vim not found in the current directory \($initvim\).
        echo Cannot create a symlink for nvim.
    fi
}

# Setup LightDM
function setupLightDM() {
    yay -S lightdm lightdm-webkit2-greeter
}

# Setup XMonad
function setupXmonad() {
    if [ -f /bin/xmonad ];
    then
        echo xmonad alread installed.
    else
        echo Installing xmonad.
        sudo pacman -S xmonad
    fi

    local xmonadhs="$PWD/xmonad.hs"
    local xmonadcfg="$HOME/.xmonad"

    if [ -e xmonadhs ];
    then
        echo xmonad.hs exists. Creating a symlink.
        ln -sf $xmonadhs $xmonadcfg

        echo Recompiling Xmonad.
        xmonad --recompile
    else
        echo xmonad.sh not found in the current directory \($xmonadhs\).
        echo Cannot create a symlink for xmonad.
    fi
}

setupNvim
setupLightDM
setupXmonad
